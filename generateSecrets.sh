#!/bin/bash

cat <<EOF > secrets.yaml
http_password: $HTTP_PASSWORD
zwave_key: $ZWAVE_KEY
home_latitude: $HOME_LATITUDE
home_longitude: $HOME_LONGITUDE
home_elevation: $HOME_ELEVATION
open_weather_api_key: $OPEN_WEATHER_API_KEY
brad_pushbullet_api_key: $BRAD_PUSHBULLET_API_KEY
jess_pushbullet_api_key: $JESS_PUSHBULLET_API_KEY
the_base_url: https://${DEPLOY_HOST}:8123
router_ip: $ROUTER_IP
router_password: $ROUTER_PASSWORD
brad_phone_mac: $BRAD_PHONE_MAC
jess_phone_mac: $JESS_PHONE_MAC
ssl_cert_location: $SSL_CERT_LOCATION
ssl_key_location: $SSL_KEY_LOCATION
EOF
