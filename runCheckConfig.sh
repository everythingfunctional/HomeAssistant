#!/bin/bash

git fetch
git checkout origin/master
source /srv/homeassistant/bin/activate
hass --script check_config
RESULT=$?
git checkout master
exit $RESULT
