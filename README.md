# Configuration For Home Assistant

[![pipeline status](https://gitlab.com/everythingfunctional/HomeAssistant/badges/master/pipeline.svg)](https://gitlab.com/everythingfunctional/HomeAssistant/commits/master)

My configuration files for Home Assistant home automation hub.
